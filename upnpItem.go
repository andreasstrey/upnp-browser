package main

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/antchfx/xmlquery"
	"github.com/bcurren/go-ssdp"
)

// UPnPItem is a container which could be a device, directory or item
type UPnPItem struct {
	Name                       string
	BaseURI                    string
	DescriptionURI             string
	IconURI                    string
	DownloadURL                string
	ContentDirectoryControlURI string
	ObjectID                   string
	ProtocolInfo               string
	Resolution                 string
	Size                       string
	Bitrate                    string
	Duration                   string
	IsMediaServer              bool
	IsDirectory                bool
	IsDevice                   bool
	IsFile                     bool
}

//DescriptionURL returns the absolute URL for the device description
func (item UPnPItem) DescriptionURL() string {
	return item.BaseURI + item.DescriptionURI
}

//IconURL returns the absolute URL for the device icon
func (item UPnPItem) IconURL() string {
	return item.BaseURI + item.IconURI
}

//ContentDirectoryControlURL returns the absolute URL for the device content directory control
func (item UPnPItem) ContentDirectoryControlURL() string {
	return item.BaseURI + item.ContentDirectoryControlURI
}

//a comment
func (item UPnPItem) LoadContentDirectory(startIndex int) []UPnPItem {
	items := make([]UPnPItem, 0, 5)
	resp, err := item.requestBrowseContentDirectory(startIndex)
	if err == nil {
		defer resp.Body.Close()
		doc, _ := xmlquery.Parse(resp.Body)

		items = append(items, handelResult(doc, item)...)
		items = append(items, handleWrongReturnCount(doc, startIndex, item)...)
	}
	return items
}

func (item UPnPItem) requestBrowseContentDirectory(startIndex int) (resp *http.Response, err error) {
	client := &http.Client{}
	requestBody := strings.NewReader(createBrowseContentDirectorRequestBody(item.ObjectID, startIndex))
	req, err := http.NewRequest("POST", item.ContentDirectoryControlURL(), requestBody)
	req.Header.Add("Content-Type", "text/xml;charset=\"utf-8\"")
	req.Header.Add("SOAPACTION", "\"urn:schemas-upnp-org:service:ContentDirectory:1#Browse\"")
	resp, err = client.Do(req)
	return
}

func createBrowseContentDirectorRequestBody(objectID string, startIndex int) string {
	return `<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				<s:Body>
					<u:Browse xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1">
						<ObjectID>` + objectID + `</ObjectID>
						<BrowseFlag>BrowseDirectChildren</BrowseFlag>
						<Filter>*</Filter>
						<StartingIndex>` + strconv.Itoa(startIndex) + `</StartingIndex>
						<RequestedCount>1000</RequestedCount>
						<SortCriteria></SortCriteria>
					</u:Browse>
				</s:Body>
			</s:Envelope>`
}

func processBrowsingResult(upnpDevice UPnPItem, result string) []UPnPItem {
	doc, _ := xmlquery.Parse(strings.NewReader(result))
	containerList := xmlquery.Find(doc, "//DIDL-Lite/container")
	itemList := xmlquery.Find(doc, "//DIDL-Lite/item")
	subItems := make([]UPnPItem, 0, len(containerList)+len(itemList))
	for _, container := range containerList {
		subItem := UPnPItem{}
		subItem.BaseURI = upnpDevice.BaseURI
		subItem.ContentDirectoryControlURI = upnpDevice.ContentDirectoryControlURI
		subItem.IsDirectory = true
		subItem.Name = container.SelectElement("dc:title").InnerText()
		subItem.ObjectID = container.SelectAttr("id")
		subItems = append(subItems, subItem)
	}
	for _, item := range itemList {
		subItem := UPnPItem{}
		subItem.BaseURI = upnpDevice.BaseURI
		subItem.ContentDirectoryControlURI = upnpDevice.ContentDirectoryControlURI
		subItem.IsDirectory = false
		subItem.Name = item.SelectElement("dc:title").InnerText()
		subItem.ProtocolInfo = item.SelectElement("res").SelectAttr("protocolInfo")
		subItem.Resolution = item.SelectElement("res").SelectAttr("resolution")
		subItem.Size = item.SelectElement("res").SelectAttr("size")
		subItem.Bitrate = item.SelectElement("res").SelectAttr("bitrate")
		subItem.Duration = item.SelectElement("res").SelectAttr("duration")
		subItem.DownloadURL = item.SelectElement("res").InnerText()
		subItem.IsFile = true
		subItems = append(subItems, subItem)
	}

	return subItems
}

func handelResult(doc *xmlquery.Node, item UPnPItem) (items []UPnPItem) {
	result := xmlquery.FindOne(doc, "//s:Envelope/s:Body/u:BrowseResponse/Result")
	if result != nil {
		resultUnescaped, err := url.QueryUnescape(result.InnerText())
		if err == nil {
			items = processBrowsingResult(item, resultUnescaped)
		}
	}
	return
}

func handleWrongReturnCount(doc *xmlquery.Node, startIndex int, item UPnPItem) (items []UPnPItem) {
	numberReturned, _ := strconv.Atoi(xmlquery.FindOne(doc, "//s:Envelope/s:Body/u:BrowseResponse/NumberReturned").InnerText())
	totalMatches, _ := strconv.Atoi(xmlquery.FindOne(doc, "//s:Envelope/s:Body/u:BrowseResponse/TotalMatches").InnerText())
	if numberReturned+startIndex < totalMatches {
		items = item.LoadContentDirectory(startIndex + numberReturned)
	}
	return
}

func searchMediaServer() []UPnPItem {
	var mediaServers []UPnPItem
	responses, err := ssdp.Search("upnp:rootdevice", 3*time.Second)
	if err == nil {
		for _, response := range responses {
			upnpDevice := readMediaServer(response.Location)
			if upnpDevice.IsMediaServer {
				mediaServers = append(mediaServers, upnpDevice)
			}
		}
	}
	return mediaServers
}

func readMediaServer(descriptionURL *url.URL) UPnPItem {
	upnpDevice := UPnPItem{}
	upnpDevice.BaseURI = descriptionURL.Scheme + "://" + descriptionURL.Hostname() + ":" + descriptionURL.Port()
	upnpDevice.DescriptionURI = descriptionURL.Path

	doc, err := xmlquery.LoadURL(descriptionURL.String())
	if err == nil {
		upnpDevice.Name = parseFriendlyName(doc)
		upnpDevice.IconURI = parseBiggestPngIcon(doc)
		upnpDevice.ContentDirectoryControlURI = parseContentDirectoryControlURL(doc)
		upnpDevice.IsMediaServer = parseMediaServer(doc)
		upnpDevice.IsDevice = true
		upnpDevice.ObjectID = "0"
	} else {
		fmt.Println(err)
	}
	return upnpDevice
}

func parseFriendlyName(doc *xmlquery.Node) (friendlyName string) {
	friendlyNameElement := xmlquery.FindOne(doc, "//root/device/friendlyName")
	if friendlyNameElement != nil {
		friendlyName = friendlyNameElement.InnerText()
	}

	return
}

func parseBiggestPngIcon(doc *xmlquery.Node) string {
	iconList := xmlquery.Find(doc, "//root/device/iconList/icon[mimetype='image/png']")
	iconSize := 0
	iconURL := ""
	for _, icon := range iconList {
		if widthElement := icon.SelectElement("width"); widthElement != nil {
			width, _ := strconv.Atoi(widthElement.InnerText())
			if iconSize < width {
				iconSize = width
				if urlElement := icon.SelectElement("url"); urlElement != nil {
					iconURL = urlElement.InnerText()
				}
			}
		}
	}

	return iconURL
}

func parseContentDirectoryControlURL(doc *xmlquery.Node) (contentDirectoryURL string) {
	contentDirectoryServiceElement := xmlquery.FindOne(doc, "//root/device/serviceList/service[serviceType='urn:schemas-upnp-org:service:ContentDirectory:1']")
	if contentDirectoryServiceElement != nil {
		if contentDirectoryControlURLElement := contentDirectoryServiceElement.SelectElement("controlURL"); contentDirectoryControlURLElement != nil {
			contentDirectoryURL = contentDirectoryControlURLElement.InnerText()
		}
	}

	return
}

func parseMediaServer(doc *xmlquery.Node) bool {
	return xmlquery.FindOne(doc, "//root/device[deviceType='urn:schemas-upnp-org:device:MediaServer:1']") != nil
}
