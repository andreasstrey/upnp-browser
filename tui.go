package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/gdamore/tcell"
	"github.com/mitchellh/go-homedir"
	"github.com/rivo/tview"
)

const rootNodeText = "Devices"

func createTUI() {
	app := tview.NewApplication()

	details := intializeDetailsView()
	footer := initializeFooter()
	directoryTreeView := initializeDirectoryTreeView(details, footer)
	grid := initializeGrid(directoryTreeView, details, footer)

	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey { return handleInput(directoryTreeView, event) })

	devicesChan := make(chan UPnPItem)
	doneChan := make(chan bool)

	go loadDevices(devicesChan, doneChan)
	go loadingEventLoop(directoryTreeView, app, devicesChan, doneChan)

	if err := app.SetRoot(grid, true).SetFocus(directoryTreeView).Run(); err != nil {
		panic(err)
	}
}

func loadDevices(devicesChan chan UPnPItem, doneChan chan bool) {
	for _, mediaServer := range searchMediaServer() {
		devicesChan <- mediaServer
	}
	doneChan <- true
}

func loadingEventLoop(directoryTreeView *tview.TreeView, app *tview.Application, devicesChan chan UPnPItem, doneChan chan bool) {
	directoryTreeView.GetRoot().ClearChildren()
	directoryTreeView.GetRoot().SetText(rootNodeText + " - Loading")
	count := 0
	for {
		select {
		case device := <-devicesChan:
			deviceNode := tview.NewTreeNode(device.Name).
				SetReference(device).
				SetColor(tcell.ColorYellow)
			directoryTreeView.GetRoot().AddChild(deviceNode)
			app.Draw()
		case <-time.NewTicker(time.Millisecond * 400).C:
			count++
			directoryTreeView.GetRoot().SetText(rootNodeText + " - Loading" + strings.Repeat(".", count%4))
			app.Draw()
		case <-doneChan:
			directoryTreeView.GetRoot().SetText(rootNodeText)
			return
		}
	}
}
func intializeDetailsView() *tview.Form {
	form := tview.NewForm()
	form.SetBorder(true).SetTitle("Details").SetTitleAlign(tview.AlignLeft)
	return form
}

func initializeDirectoryTreeView(form *tview.Form, footer *tview.TextView) *tview.TreeView {
	directoryTreeView := tview.NewTreeView()
	rootNode := initializeRootNode()
	directoryTreeView.SetRoot(rootNode)
	directoryTreeView.SetCurrentNode(rootNode)
	directoryTreeView.SetSelectedFunc(handleSelection)
	directoryTreeView.SetChangedFunc(func(node *tview.TreeNode) { handleCurrentNodeChanged(form, footer, node) })
	return directoryTreeView
}

func initializeRootNode() *tview.TreeNode {
	rootNode := tview.NewTreeNode(rootNodeText)
	rootNode.SetColor(tcell.ColorRed)
	return rootNode
}

func initializeFooter() *tview.TextView {
	return tview.NewTextView().SetDynamicColors(true).SetText("[black:white]^C[white:black] Close")
}

func initializeGrid(directoryTreeView *tview.TreeView, details *tview.Form, footer *tview.TextView) *tview.Grid {
	grid := tview.NewGrid()
	grid.SetRows(1, 0, 1)
	grid.SetColumns(0, 50)
	grid.AddItem(tview.NewTextView().SetTextAlign(tview.AlignCenter).SetText("UPnP Browser"), 0, 0, 1, 2, 0, 0, true)
	grid.AddItem(directoryTreeView, 1, 0, 1, 1, 0, 0, true)
	grid.AddItem(details, 1, 1, 1, 1, 0, 0, true)
	grid.AddItem(footer, 2, 0, 1, 2, 0, 0, true)
	return grid
}
func add(target *tview.TreeNode, upnpItem UPnPItem) {
	subItems := upnpItem.LoadContentDirectory(0)
	for _, item := range subItems {
		node := tview.NewTreeNode(item.Name).
			SetReference(item)
		if item.IsDirectory {
			node.SetColor(tcell.ColorGreen)
		}
		target.AddChild(node)
	}
}

func handleSelection(node *tview.TreeNode) {
	reference := node.GetReference()
	if reference == nil {
		return
	}
	children := node.GetChildren()
	if node.IsExpanded() && len(children) > 0 {
		node.ClearChildren()
	} else {
		item := reference.(UPnPItem)
		if item.IsDirectory || item.IsDevice {
			add(node, item)
		}
	}
}

func handleCurrentNodeChanged(form *tview.Form, footer *tview.TextView, node *tview.TreeNode) {
	form.Clear(true)
	if node.GetReference() != nil {
		item := node.GetReference().(UPnPItem)
		form.AddInputField("Name", item.Name[0:min(30, len(item.Name))], 30, nil, nil)
		form.AddInputField("ProtocolInfo", item.ProtocolInfo, 30, nil, nil)
		form.AddInputField("Resolution", item.Resolution, 30, nil, nil)
		size, err := strconv.Atoi(item.Size)
		if err == nil {
			form.AddInputField("Size", fmt.Sprintf("%.1f MB", float32(size)/1024/1024), 30, nil, nil)
		}
		form.AddInputField("Bitrate", item.Bitrate, 30, nil, nil)
		form.AddInputField("Duration", item.Duration, 30, nil, nil)
		if item.IsFile {
			footer.SetText("[black:white]^C[white:black] Close\t[black:white]^D[white:black] Download File")
		} else {
			footer.SetText("[black:white]^C[white:black] Close")
		}
	}
}

func handleInput(directoryTreeView *tview.TreeView, event *tcell.EventKey) *tcell.EventKey {
	switch key := event.Key(); key {
	case tcell.KeyCtrlD:
		reference := directoryTreeView.GetCurrentNode().GetReference()
		if reference != nil && reference.(UPnPItem).IsFile {
			err := downloadFile(reference.(UPnPItem).DownloadURL)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
		return nil
	}

	return event
}

func downloadFile(url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	homedir, err := homedir.Dir()
	if err != nil {
		return err
	}

	out, err := os.Create(homedir + "/Downloads/" + path.Base(resp.Request.URL.String()))
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func min(x, y int) int {
	if x > y {
		return y
	}
	return x
}
